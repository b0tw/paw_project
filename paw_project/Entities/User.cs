﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paw_project.Entities
{
    [Serializable]
    public class User
    {
        public long IdUser { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool ParentalControl { get; set; }

        public User(string userName, string email, bool parentalControl)
        {
            UserName = userName;
            Email = email;
            ParentalControl = parentalControl;
        }

        public User(long idUser, string userName, string email, bool parentalControl) : this(userName,email,parentalControl)
        {
            IdUser = idUser;
        }

        public User()
        {
        }
    }
}
