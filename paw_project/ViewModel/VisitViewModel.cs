﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace paw_project.ViewModel
{
    internal class VisitViewModel : INotifyPropertyChanged
    {
        private string _idUser;
        public string IdUser
        {
            get { return _idUser; }
            set
            {
                if (_idUser == value)
                    return;
                _idUser = value;
                OnPropertyChanged("IdUser");
            }
        }
        private string _domain;
        public string Domain {
            get { return _domain; }
            set
            {
                if (_domain == value)
                    return;
                _domain = value;
                OnPropertyChanged("Domain");
            }
        }
        private DateTime _time;
        public DateTime Time {
            get { return _time; }
            set
            {
                if (_time == value)
                    return;
                _time = value;
                OnPropertyChanged("Time");
            }
        }

        public BindingList<Visit> Visits { get; set; }

        public VisitViewModel()
        {
            Visits = new BindingList<Visit>();
            Time = DateTime.Now;
        }
        public void AddVisit()
        {
            Visits.Add(new Visit(Time, IdUser, Domain));
            Time = DateTime.Now;
            IdUser = Domain = string.Empty;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #region INotifyPropertyChanged
        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
