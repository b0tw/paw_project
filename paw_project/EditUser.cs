﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paw_project
{
    
    public partial class EditUser : Form
    {
        private readonly User _user;
        public EditUser(User user)
        {
            InitializeComponent();
            _user = user;
        }

        private void EditUser_Load(object sender, EventArgs e)
        {
            tbEmail.Text = _user.Email;
            tbUserName.Text = _user.UserName;
            if (_user.ParentalControl)
                cbParentalControl.Checked = true;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _user.Email = tbEmail.Text;
            _user.UserName = tbUserName.Text;
            _user.ParentalControl = cbParentalControl.Checked;

        }
    }
}
