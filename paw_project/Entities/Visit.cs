﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paw_project.Entities
{
    public class Visit
    {
        public DateTime Time { get; set; }
        public string IdUser { get; set; } 
        public string Domain { get; set; }

        public Visit(DateTime time, string idUser, string domain)
        {
            Time = time;
            IdUser = idUser;
            Domain = domain;
        }

        public Visit()
        {
        }
    }
}
