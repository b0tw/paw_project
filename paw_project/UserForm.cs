﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace paw_project
{
    public partial class UserForm : Form
    {
        private List<User> Users;
        private const string ConnectionString = "Data Source=database.db";
        public UserForm()
        {
            InitializeComponent();
            Users = new List<User>();
        }

        private void UserFormClear()
        {
            tbUserName.Clear();
            tbEmail.Clear();
            cbParentalControl.Checked = false;
        }

        public void DisplayUsers()
        {
            lvUser.Items.Clear();
            foreach (var user in Users)
            {
                ListViewItem item = new ListViewItem(user.UserName);
                item.SubItems.Add(user.Email);
                item.SubItems.Add(user.ParentalControl.ToString());

                item.Tag = user;

                lvUser.Items.Add(item);
            }
        }
        private bool isUserNameValid()
        {
            return !string.IsNullOrWhiteSpace(tbUserName.Text.Trim());
        }
        private bool isEmailValid()
        {
            Regex reg = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
            return reg.IsMatch(tbEmail.Text);

        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!ValidateChildren())
            {
                MessageBox.Show("The form contains errors", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string userName = tbUserName.Text;
            string email = tbEmail.Text;
            bool parentalControl;
            if (cbParentalControl.Checked)
                parentalControl = true;
            else
                parentalControl = false;
            UserFormClear();
            var user = new User(userName, email, parentalControl);
            Users.Add(user);
            DisplayUsers();
        }

        private void tbUserName_Validating(object sender, CancelEventArgs e)
        {
            if (!isUserNameValid())
            {
                e.Cancel = true;
                errorProvider.SetError((Control)sender, "User name is invalid");
            }
        }

        private void tbEmail_Validating(object sender, CancelEventArgs e)
        {
            if (!isEmailValid())
            {
                e.Cancel = true;
                errorProvider.SetError((Control)sender, "Not a valid email address!");
            }
        }

        private void tbEmail_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((Control)sender, string.Empty);
        }

        private void tbUserName_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((Control)sender, string.Empty);
        }

        private void serializeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream s = File.Create("users.bin"))
                formatter.Serialize(s, Users);
        }

        private void deserializeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream s = File.OpenRead("users.bin"))
            {
                Users = (List<User>)formatter.Deserialize(s);
                DisplayUsers();
            }
        }

        private void serializeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
            using (FileStream s = File.Create("users.xml"))
                serializer.Serialize(s, Users);

        }

        private void deserializeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
            using (FileStream s = File.OpenRead("users.xml"))
            {
                Users = (List<User>)serializer.Deserialize(s);
                DisplayUsers();
            }
        }

        private void btnDeleteUsers_Click(object sender, EventArgs e)
        {
            if (lvUser.SelectedItems.Count == 0)
            {
                MessageBox.Show("Choose a user");
                return;
            }

            if (MessageBox.Show("Are you sure?", "Delete user", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Users.Remove((User)lvUser.SelectedItems[0].Tag);
                DisplayUsers();
            }
        }

        private void btnEditUsers_Click(object sender, EventArgs e)
        {
            if (lvUser.SelectedItems.Count == 0)
            {
                MessageBox.Show("Select an user");
                return;
            }
            EditUser editUser = new EditUser((User)lvUser.SelectedItems[0].Tag);
            if (editUser.ShowDialog() == DialogResult.OK)
                DisplayUsers();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file | *.txt";
            saveFileDialog.Title = "Save as text file";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
                using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                {
                    sw.WriteLine("User name,Email,Parental Control");
                    foreach (var user in Users)
                        sw.WriteLine("{0},{1},{2}", user.UserName, user.Email, user.ParentalControl.ToString());
                }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open file";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(openFileDialog.FileName))
                {
                    //read the first line with the name of the categories
                    sr.ReadLine();
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] lineArray = line.Split(',');
                        string userName = lineArray[0];
                        string email = lineArray[1];
                        bool parentalControl = Boolean.Parse(lineArray[2]);
                        User user = new User(userName, email, parentalControl);
                        Users.Add(user);
                    }
                    DisplayUsers();

                }
            }
        }
        private void AddUserDB(User user)
        {
            var queryString = "Insert into User(UserName,Email,ParentalControl)" +
                "values(@userName,@email,@parentalControl); " + "SELECT last_insert_rowid();";
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                var command = new SQLiteCommand(queryString, connection);
                command.Parameters.AddWithValue("@userName", user.UserName);
                command.Parameters.AddWithValue("@email", user.Email);
                command.Parameters.AddWithValue("@parentalControl", user.ParentalControl);
                user.IdUser = (long)command.ExecuteScalar();

            }
        }
        private void ReadUsersDB()
        {
            var queryString = "SELECT * FROM USER;";
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                var command = new SQLiteCommand(queryString, connection);
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        long id = (long)reader["IdUser"];
                        string userName = (string)reader["UserName"];
                        string email = (string)reader["Email"];
                        string pA = (string)reader["ParentalControl"];
                        bool parentalControl;
                        if (pA == "1")
                            parentalControl = true;
                        else
                            parentalControl = false;
                        User user = new User(id, userName, email, parentalControl);
                        Users.Add(user);
                    }
                    DisplayUsers();
                }
            }
        }
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ReadUsersDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var user in Users)
                    AddUserDB(user);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
