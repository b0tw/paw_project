﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace paw_project
{
    public partial class WebsiteForm : Form
    {
        private List<WebsiteCategory> Websites;
        private const string ConnectionString = "Data Source=database.db";

        public WebsiteForm()
        {
            InitializeComponent();
            Websites = new List<WebsiteCategory>();
        }
        private void clearWebsite()
        {
            tbCategory.Clear();
            tbDomain.Clear();
            cbParentalAdvisory.Checked = false;
        }
        private bool isDomainValid()
        {
            return !string.IsNullOrWhiteSpace(tbDomain.Text.Trim());
        }
        private bool isValidCategory()
        {
            return !string.IsNullOrWhiteSpace(tbCategory.Text.Trim());
        }
        private void DisplayWebsites()
        {
            lvWebsite.Items.Clear();
            foreach (var website in Websites)
            {
                ListViewItem viewItem = new ListViewItem(website.Domain);
                viewItem.SubItems.Add(website.Category);
                viewItem.SubItems.Add(website.ParentalAdvisory.ToString());

                viewItem.Tag = website;
                lvWebsite.Items.Add(viewItem);
            }
        }
        private void btnAddWebsite_Click(object sender, EventArgs e)
        {

            if (!ValidateChildren())
            {
                MessageBox.Show("Form has errors", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string domain = tbDomain.Text;
            string category = tbCategory.Text;
            bool parentalAdvisory = cbParentalAdvisory.Checked;

            clearWebsite();

            WebsiteCategory websiteCategory = new WebsiteCategory(domain, category, parentalAdvisory);
            Websites.Add(websiteCategory);

            DisplayWebsites();
        }

        private void tbDomain_Validating(object sender, CancelEventArgs e)
        {
            if (!isDomainValid())
            {
                e.Cancel = true;
                errorProvider.SetError((Control)sender, "Invalid domain name");
                return;
            }
        }

        private void tbDomain_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((Control)sender, string.Empty);
        }

        private void tbCategory_Validating(object sender, CancelEventArgs e)
        {
            if (!isValidCategory())
            {
                e.Cancel = true;
                errorProvider.SetError((Control)sender, "Category is invalid");
            }
        }

        private void tbCategory_Validated(object sender, EventArgs e)
        {
            errorProvider.SetError((Control)sender, string.Empty);
        }

        private void serializeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream s = File.Create("website.bin"))
                formatter.Serialize(s, Websites);
        }

        private void deserializeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream s = File.OpenRead("website.bin"))
            {
                Websites = (List<WebsiteCategory>)formatter.Deserialize(s);
                DisplayWebsites();
            }
        }

        private void serializeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<WebsiteCategory>));
            using (FileStream s = File.Create("website.xml"))
                serializer.Serialize(s, Websites);
        }

        private void deserializeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<WebsiteCategory>));
            using (FileStream s = File.OpenRead("website.xml"))
            {
                Websites = (List<WebsiteCategory>)serializer.Deserialize(s);
                DisplayWebsites();
            }
        }

        private void btnDeleteWebsite_Click(object sender, EventArgs e)
        {
            if (lvWebsite.SelectedItems.Count != 1)
            {
                MessageBox.Show("Choose one website");
                return;
            }
            if (MessageBox.Show("Are you sure?", "Delete website", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Websites.Remove((WebsiteCategory)lvWebsite.SelectedItems[0].Tag);
                DisplayWebsites();
            }
        }

        private void btnEditWebsite_Click(object sender, EventArgs e)
        {
            if (lvWebsite.SelectedItems.Count != 1)
            {
                MessageBox.Show("Choose one website");
                return;
            }

            EditWebsite editWebsite = new EditWebsite((WebsiteCategory)lvWebsite.SelectedItems[0].Tag);

            if (editWebsite.ShowDialog() == DialogResult.OK)
                DisplayWebsites();

        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File | *.txt";
            saveFileDialog.Title = "Save as text file";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                {
                    sw.WriteLine("Domain,Category,ParentalAdvisory");
                    foreach (var website in Websites)
                        sw.WriteLine("{0},{1},{2}", website.Domain, website.Category, website.ParentalAdvisory.ToString());

                }
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open file";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(openFileDialog.FileName))
                {
                    sr.ReadLine();
                    while (!sr.EndOfStream)
                    {
                        string[] line = sr.ReadLine().Split(',');
                        string domain = line[0];
                        string category = line[1];
                        bool parentalAdvisory = Boolean.Parse(line[2]);

                        WebsiteCategory website = new WebsiteCategory(domain, category, parentalAdvisory);
                        Websites.Add(website);
                    }
                    DisplayWebsites();
                }
            }
        }
        private void AddWebsiteDB(WebsiteCategory website)
        {
            var query = "insert into Website(Domain,Category,ParentalAdvisory)"
                + "values(@domain,@category,@parentalAdvisory);";
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand(query, connection);
                command.Parameters.AddWithValue("@domain", website.Domain);
                command.Parameters.AddWithValue("@category", website.Category);
                command.Parameters.AddWithValue("@parentalAdvisory", website.ParentalAdvisory);
                command.ExecuteScalar();
            }
        }

        private void ReadWebsitesDB()
        {
            var query = "select * from Website";
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand(query, connection);
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        long id = (long)reader["IdWebsite"];
                        string domain = (string)reader["Domain"];
                        string category = (string)reader["Category"];
                        bool parentalAdvisory;
                        string pA = (string)reader["ParentalAdvisory"];
                        if (pA == "1")
                            parentalAdvisory = true;
                        else
                            parentalAdvisory = false;
                        WebsiteCategory website = new WebsiteCategory(id, domain, category, parentalAdvisory);
                        Websites.Add(website);
                    }
                }
            }
        }
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ReadWebsitesDB();
                DisplayWebsites();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (var website in Websites)
                    AddWebsiteDB(website);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
