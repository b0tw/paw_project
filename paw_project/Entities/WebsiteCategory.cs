﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paw_project.Entities
{
    [Serializable]
    public class WebsiteCategory
    {
        public long IdWebsite;
        public string Domain;
        public string Category;
        public bool ParentalAdvisory;

        public WebsiteCategory(string domain, string category, bool parentalAdvisory)
        {
            Domain = domain;
            Category = category;
            ParentalAdvisory = parentalAdvisory;
        }

        public WebsiteCategory(long idWebsite, string domain, string category, bool parentalAdvisory)
        : this(domain, category, parentalAdvisory)
        {
            IdWebsite = idWebsite;
        }

        public WebsiteCategory()
        {
        }
    }
}
