﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paw_project
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            UserForm userForm = new UserForm();
            userForm.ShowDialog();
        }

        private void btnWebsite_Click(object sender, EventArgs e)
        {
            WebsiteForm websiteForm = new WebsiteForm();
            websiteForm.ShowDialog();
        }

        private void btnVisit_Click(object sender, EventArgs e)
        {
            VisitForm visitForm = new VisitForm();
            visitForm.ShowDialog();
        }
    }
}
