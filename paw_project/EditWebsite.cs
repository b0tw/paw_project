﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paw_project
{
    public partial class EditWebsite : Form
    {
        private readonly WebsiteCategory _website;
        public EditWebsite(WebsiteCategory website)
        {
            InitializeComponent();
            _website = website;
        }

        private void EditWebsite_Load(object sender, EventArgs e)
        {
            tbDomain.Text = _website.Domain;
            tbCategory.Text = _website.Category;
            cbParentalAdvisory.Checked = _website.ParentalAdvisory;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _website.Domain = tbDomain.Text;
            _website.Category = tbCategory.Text;
            _website.ParentalAdvisory = cbParentalAdvisory.Checked;
        }
    }
}
