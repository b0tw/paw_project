﻿using paw_project.Entities;
using paw_project.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paw_project
{
    public partial class VisitForm : Form
    {
        private readonly VisitViewModel _viewModel;
        public VisitForm()
        {
            InitializeComponent();
            Load += VisitForm_Load;

            _viewModel = new VisitViewModel();
        }
        private void btnAddVisit_Click(object sender, EventArgs e)
        {
            _viewModel.AddVisit();
        }

        private void VisitForm_Load(object sender, EventArgs e)
        {
            dgvVisits.DataSource = _viewModel.Visits;

            tbIdUser.DataBindings.Clear();
            tbDomain.DataBindings.Clear();
            dtpTime.DataBindings.Clear();

            tbIdUser.DataBindings.Add("Text", _viewModel, nameof(VisitViewModel.IdUser));
            tbDomain.DataBindings.Add("Text", _viewModel, nameof(VisitViewModel.Domain));
            dtpTime.DataBindings.Add("Value", _viewModel, nameof(VisitViewModel.Time));

        }

        private void btnDeleteVisit_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvVisits.SelectedRows)
            {
                dgvVisits.Rows.RemoveAt(row.Index);
            }
        }

        private void btnEditVisit_Click(object sender, EventArgs e)
        {

            EditVisit editVisit = new EditVisit((Visit)dgvVisits.SelectedRows[0].DataBoundItem);

        }
    }
}
