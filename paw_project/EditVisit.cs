﻿using paw_project.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paw_project
{
    public partial class EditVisit : Form
    {
        private readonly Visit visit;
        public EditVisit(Visit _visit)
        {
            InitializeComponent();
            visit = _visit;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            visit.IdUser = tbIdUser.Text;
            visit.Domain = tbDomain.Text;
            visit.Time = dtpTime.Value;
        }

        private void EditVisit_Load(object sender, EventArgs e)
        {
            tbIdUser.Text = visit.IdUser;
            tbDomain.Text = visit.Domain;
            dtpTime.Value = visit.Time;
        }
    }
}
